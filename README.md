# Тестовое задание
Взять open-source приложение [outline](https://www.getoutline.com/) и развернуть его с зависимостями (redis + minio) в готовом кластере к8с (postgres внешний). 
В качестве ingress использовать nginx, добавить ssl letsencrypt-staging.
Postgresql доступнен только из сети k8s
